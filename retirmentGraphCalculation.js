function calculateRetirementGraphValues(
  annualGrowthRateTillRetire,
  annualGrowthRateAfterRetire,
  netWorthToday,
  todayAge,
  lifeExpectancy,
  retirementAge,
  windFallAmount,
  windFallYear,
  majorExpenseAmount,
  expenseYear
) {
  var growthRateTillRetire = (100 + annualGrowthRateTillRetire) / 100;
  var growthRateAfterRetire = (100 + annualGrowthRateAfterRetire) / 100;
  var growthRate = growthRateTillRetire;
  var p = netWorthToday;
  var n = todayAge;
  var data = [];

  for (let age = todayAge; age < lifeExpectancy; age++) {
    if (age > retirementAge) {
      if (p < 0) {
        growthRate = (100 + annualGrowthRateAfterRetire * -1) / 100;
      } else {
        growthRate = (100 + annualGrowthRateAfterRetire) / 100;
      }
      var netWorthAtAge = p * (growthRate / 100) * (age - n);
      if (
        windFallAmount > 0 &&
        age == windFallYear + 1 &&
        majorExpenseAmount > 0 &&
        age == expenseYear + 1
      ) {
        p = netWorthAtAge + (windFallAmount - majorExpenseAmount);
        n = age;
        data.push({ age, investment: p });
      } else if (windFallAmount > 0 && age == windFallYear + 1) {
        p = netWorthAtAge + windFallAmount;
        n = age;
        data.push({ age, investment: p });
      } else if (majorExpenseAmount > 0 && age == expenseYear + 1) {
        p = netWorthAtAge - majorExpenseAmount;
        n = age;
        data.push({ age, investment: p });
      } else {
        data.push({ age, investment: netWorthAtAge });
      }
    } else {
      if (p < 0) {
        growthRate = (100 + annualGrowthRateTillRetire * -1) / 100;
      } else {
        growthRate = (100 + annualGrowthRateTillRetire) / 100;
      }
      var netWorthAtAge = p * (growthRate / 100) * (age - n);
      if (
        windFallAmount > 0 &&
        age == windFallYear + 1 &&
        majorExpenseAmount > 0 &&
        age == expenseYear + 1
      ) {
        p = netWorthAtAge + (windFallAmount - majorExpenseAmount);
        n = age;
        data.push({ age, investment: p });
      } else if (windFallAmount > 0 && age == windFallYear + 1) {
        p = netWorthAtAge + windFallAmount;
        n = age;
        data.push({ age, investment: p });
      } else if (majorExpenseAmount > 0 && age == expenseYear + 1) {
        p = netWorthAtAge - majorExpenseAmount;
        n = age;
        data.push({ age, investment: p });
      } else {
        if (age == retirementAge) {
          p = netWorthAtAge;
          n = retirementAge;
        }
        data.push({ age, investment: netWorthAtAge });
      }
    }
  }
  console.log("data", data);
  return data;
}

// calculateRetirementGraphValues(
//   "5",
//   "1.5",
//   93055.91,
//   28,
//   93,
//   77,
//   22222,
//   42,
//   0,
//   27
// );
