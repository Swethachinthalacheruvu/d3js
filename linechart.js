var margin = {
    top: 40,
    right: 50,
    bottom: 50,
    left: 60,
  },
  width = 900 - margin.left - margin.right,
  height = 650 - margin.top - margin.bottom;
//append the svg object to the body of the page
var svg = d3
  .select("#my_dataviz")
  .append("svg")
  .attr("width", width + margin.left + margin.right)
  .attr("height", height + margin.top + margin.bottom)
  .append("g")
  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

//this will select the closest age on the x axis when a user hover over the chart
var bisectage = d3.bisector((d) => d.age).left;
//set the width of the variable
const tooltipWidth = 300;
//Read the data
var result = calculateRetirementGraphValues(
  "5",
  "1.5",
  93055.91,
  28,
  93,
  77,
  22222,
  42,
  0,
  27
);

d3.csv(
  "data.csv",

  (d) => {
    return {
      age: +d.age,
      investment: +d.investment,
    };
  },
  function (data) {
    //add x-axis
    var x = d3
      .scaleLinear()
      .domain(
        d3.extent(data, function (d) {
          return d.age;
        })
      )
      .range([0, width]);
    svg
      .append("text")
      .attr("transform", "translate(" + 0 + " ," + (height - 20) + ")")
      .text("93K")
      .style("font", "12px times");

    xAxis = svg
      .append("g")
      .attr("class", "x-axis")
      .attr("transform", "translate(0," + height + ")")
      .call(
        d3
          .axisBottom(x)
          .ticks(3)
          .tickValues([28, 77, 93])
          .tickFormat(
            (d) =>
              "Age " +
              d +
              ` years (${
                d === 28 ? "Today" : d === 77 ? "Retirement" : "Life Expectancy"
              })`
          )
      );
    //add y-axis
    var y = d3
      .scaleLinear()

      .domain([
        d3.min(data, function (d) {
          data.forEach(function (d) {
            d.yaxis = d.yaxis * 1000;
          });
          return +d.investment;
        }),
        d3.max(data, function (d) {
          return +d.investment;
        }),
      ])
      .range([height, 0]);

    yAxis = svg
      .append("g")
      .attr("transform", "translate(790,0)")
      .call(
        d3
          .axisRight(y)
          .ticks(6)
          .tickValues([93000, 300000, 600000, 900000, 1200000, 1500000])

          .tickFormat((d) => "$" + d3.format(".2s")(d))
      );

    //add a clipPath
    var clip = svg
      .append("defs")
      .append("svg:clipPath")
      .attr("id", "clip")
      .append("svg:rect")
      .attr("width", width)
      .attr("height", height)
      .attr("x", 0)
      .attr("y", 0);

    //create the area variable
    var area = svg.append("g").attr("clip-path", "url(#clip)");
    var areaGenerator = d3
      .area()
      .x((d) => x(d.age))
      .y0(y(0))
      .y1((d) => y(d.investment))
      .curve(d3.curveCardinal);

    //draw focus object
    function drawFocus() {
      let focus = svg.append("g").attr("class", "focus");

      //add a y-line to show where hovering
      focus.append("line").classed("y", true);
      //append circle on the line path
      focus.append("circle").attr("r", 7.5);

      //add text annotation for tooltip
      focus.append("text").attr("dx", "0.002em").style("fill", "black");

      focus
        .append("div")
        .attr("x", 10)
        .attr("dy", ".35em")
        .attr("class", "tooltip")
        .style("opacity", 1);

      //create a overlay rectange to draw th above objects on top of
      svg
        .append("rect")
        .attr("class", "overlay")
        .attr("width", width)
        .attr("height", height)
        .on("mouseover", () => focus.style("display", null))
        .on("mouseout", () => focus.style("display", "none"))
        .on("mousemove", tipMove);

      //select the overlay and style it
      d3.select(".overlay")
        .style("fill", "none")
        .style("pointer-events", "all");

      d3.selectAll(".focus").style("opacity", 0.9);

      //select the circle and style it
      d3.selectAll(".focus circle")
        .style("fill", "#068ca0")
        .style("opacity", 0);

      //select the hover lines and style it
      d3.selectAll(".focus line")

        .style("fill", "none")
        .style("opacity", 0.4)
        .style("stroke", "black")
        .style("stroke-width", "1px");

      //function that adds tooltiip on hover
      function tipMove() {
        let x0 = x.invert(d3.mouse(this)[0]);
        let i = bisectage(data, x0, 1);
        let d0 = data[i - 1];
        let d1 = data[i];
        let d = x0 - d0.age > d1.age - x0 ? d1 : d0;

        focus.attr("transform", `translate(${x(d.age)},${y(d.investment)})`);
        //positioning the x line
        focus
          .select("line.x")
          .attr("x1", 0)
          .attr("x2", x(d.age))
          .attr("y1", 0)
          .attr("y2", 0);

        document.getElementById("age").innerHTML = `
          <span class="value">${
            "$" + Intl.NumberFormat("en-AU").format(Math.round(d.investment))
          } </span> 
          <span class="statement">Net worth at retirement at age ${
            d.age
          } </span> 
        `;
        //  console.log(-x(d.age));

        //positioning the y line

        focus
          .select("line.y")
          .attr("x1", 0)
          .attr("x2", 0)
          .attr("y1", height - y(d.investment))
          .attr("y2", -height);

        //positioning the y line
        focus
          .select("text")
          .attr("x", 0)
          .attr("y", height - y(d.investment) + 34)
          .text("Age " + d.age)
          .transition()
          .duration(100)
          .style("opacity", 1);

        //show the circle on the path
        focus.selectAll(".focus circle").style("opacity", 1);
      }
    }
    drawFocus();
    //add the area to the chart
    var splitAge = 76;
    var bucket = [];
    var prev = undefined;
    var datanew = [];
    data.forEach(function (d) {
      if (d.age > splitAge) {
        if (prev && prev.age <= splitAge) {
          bucket.push(d);
          datanew.push(bucket);
          bucket = [];
        }
        bucket.push(d);
      } else {
        if (prev && prev.age > splitAge) {
          datanew.push(bucket);
          bucket = [];
        }
        bucket.push(d);
      }
      prev = d;
    });
    datanew.push(bucket);

    svg
      .append("defs")
      .append("linearGradient")
      .attr("id", "areaGradient")
      .attr("x1", "0%")
      .attr("y1", "0%")
      .attr("x2", "0%")
      .attr("y2", "100%")

      .selectAll("stop")
      .data([
        { offset: "60%", color: " #004e92", opacity: 0.6 },
        { offset: "100%", color: "white", opacity: 0.4 },
      ])
      .enter()
      .append("stop")
      .attr("offset", function (d) {
        return d.offset;
      })
      .attr("stop-color", function (d) {
        return d.color;
      })
      .attr("stop-opacity", function (d) {
        return d.opacity;
      });

    svg
      .append("defs")
      .append("linearGradient")
      .attr("id", "areaGradientgreen")
      .attr("x1", "0%")
      .attr("y1", "0%")
      .attr("x2", "0%")
      .attr("y2", "100%")

      .selectAll("stop")
      .data([
        { offset: "60%", color: "#43cea2", opacity: 0.6 },
        { offset: "100%", color: "white", opacity: 0.4 },
      ])
      .enter()
      .append("stop")
      .attr("offset", function (d) {
        return d.offset;
      })
      .attr("stop-color", function (d) {
        return d.color;
      })
      .attr("stop-opacity", function (d) {
        return d.opacity;
      });

    datanew.forEach(function (d) {
      //iterate through the chunks
      // if (d[0].age >= splitAge);
      // else c = "skyblue";
      area
        .append("path")
        .datum(d)
        .attr("class", "myArea")
        .style(
          "fill",
          `${
            d[0].age >= splitAge
              ? "url(#areaGradient)"
              : "url(#areaGradientgreen)"
          }`
        )
        .attr("stroke", `${d[0].age >= splitAge ? " #004e92" : "#43cea2"}`)

        // .attr("stroke","#004e92")

        .attr("stroke-width", 1)
        .attr("d", areaGenerator);
    });
  }
);
